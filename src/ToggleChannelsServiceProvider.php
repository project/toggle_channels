<?php

namespace Drupal\toggle_channels;

use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class ToggleChannelsServiceProvider.
 */
class ToggleChannelsServiceProvider extends ServiceProviderBase implements ServiceProviderInterface {

  /**
   * {@inheritDoc}
   */
  public function alter(ContainerBuilder $container) {
    $definition = $container->getDefinition('logger.factory');
    $definition->setClass('Drupal\toggle_channels\Logger\NullableLoggerChannel');
    $definition->setArguments([
      new Reference('config.factory'),
      new Reference('logger.toggle_channels'),
    ]);
  }

}
