<?php

namespace Drupal\toggle_channels\Logger;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\toggle_channels\Form\SettingsForm;
use Psr\Log\LoggerInterface;

/**
 * Class that will disable the logging.
 */
class NullableLogger implements LoggerInterface {
  /**
   * Config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * ConfigurableLogger constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory.
   */
  public function __construct(ConfigFactoryInterface $configFactory) {
    $this->config = $configFactory->get(SettingsForm::SETTINGS);
  }

  /**
   * {@inheritDoc}
   */
  public function log($level, string|\Stringable $message, array $context = []): void {}

  /**
   * {@inheritDoc}
   */
  public function info(string|\Stringable $message, array $context = []): void {}

  /**
   * {@inheritDoc}
   */
  public function error(string|\Stringable $message, array $context = []): void {}

  /**
   * {@inheritDoc}
   */
  public function notice(string|\Stringable $message, array $context = []): void {}

  /**
   * {@inheritDoc}
   */
  public function warning(string|\Stringable $message, array $context = []): void {}

  /**
   * {@inheritDoc}
   */
  public function critical(string|\Stringable $message, array $context = []): void {}

  /**
   * {@inheritDoc}
   */
  public function alert(string|\Stringable $message, array $context = []): void {}

  /**
   * {@inheritDoc}
   */
  public function debug(string|\Stringable $message, array $context = []): void {}

  /**
   * {@inheritDoc}
   */
  public function emergency(string|\Stringable $message, array $context = []): void {}

}
