<?php

namespace Drupal\toggle_channels\Logger;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\toggle_channels\Form\SettingsForm;
use Psr\Log\LoggerInterface;
use Drupal\Core\Logger\LoggerChannelFactory;

/**
 * Class NullLogger.
 */
class NullableLoggerChannel extends LoggerChannelFactory {
  /**
   * Form config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Class constructor.
   */
  public function __construct(ConfigFactoryInterface $config, LoggerInterface $logger) {
    $this->config = $config->get(SettingsForm::SETTINGS);
    // Nullable logger.
    $this->logger = $logger;
  }

  /**
   * {@inheritDoc}
   */
  public function addLogger(LoggerInterface $logger, $priority = 0) {
    if ($this->config->get('toggle_logger')) {
      $disabledChannel = $this->config->get('toggle_channels');

      foreach ($disabledChannel as $key => $channel) {
        if ($channel !== 0) {
          $channelInstance = $this->get($channel);
          $channelInstance->setLoggers([$priority => [0 => $this->logger]]);
        }
      }
    }

    parent::addLogger($logger, $priority);
  }

}
